FROM ruby:2.7.0-alpine3.11 as builder

ENV RAILS_ENV production
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true

RUN apk add --no-cache \
  build-base \
  busybox \
  ca-certificates \
  curl \
  git \
  libffi-dev \
  libsodium-dev \
  nodejs=12.15.0-r1 \
  openssh-client \
  postgresql-dev \
  rsync \
  linux-headers

RUN mkdir -p /app
WORKDIR /app

#Trick to avoid building gems every time
COPY Gemfile /app/
COPY Gemfile.lock /app/

#build gems for production only
#RUN bundle install --without development test -j4 --retry 3 \
#  && rm -rf /usr/local/bundle/bundler/gems/*/.git \
#    /usr/local/bundle/cache/

#build gems for all environments
RUN bundle install -j4 --retry 3 \
  && rm -rf /usr/local/bundle/bundler/gems/*/.git \
    /usr/local/bundle/cache/

COPY . /app/

#Trick for assets:precompile without DB
# RUN mv db/schema.rb db/schema.rb.true
# RUN touch db/schema.rb
# RUN bundle exec rake \ 
#   DATABASE_ADAPTER=nulldb \
#   SECRET_TOKEN=adummytoken \
#   LISTEN_ON=0.0.0.0:8000 \
#   RAILS_ENV=production \
#   DATABASE=db \
#   DATABASE_USER=user \
#   DATABASE_PASS=pass \
#   assets:precompile


# Packaging final app w/o node_modules & the development tools
FROM ruby:2.7.0-alpine3.11

ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

#Maybe you can find an unneeded dependency
RUN apk add --no-cache \
  busybox \
  ca-certificates \
  curl \
  nodejs=12.15.0-r1 \
  postgresql-dev \
  rsync \
  file 

RUN mkdir -p /app
WORKDIR /app

#copy dependencies and app with its assets
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder /app/ /app/

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]