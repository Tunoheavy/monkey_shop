# DevOps-ready Shop API

This code implements a REST API to display products, purchase them and review submited orders. Data is backed on a PostgreSQL 
database and the backend is developed with Ruby On Rails. The sections of this README are sumarized below:

* [API definition](README.md#api-definition)
* [Implementation details](README.md#implementation-details)
* [Running the application locally with docker compose](README.md#running-the-application-locally-with-docker-compose)
* [Create a CloudSQL instance and a Service Account](README.md#Create-a-CloudSQL-instance-and-a-Service-Account)
* [Running the application locally with a database in CloudSQL](README.md#Running-the-application-locally-with-a-database-in-CloudSQL)
* [Running the application in Kubernetes](README.md#Running-the-application-in-Kubernetes)
* [Integrate with CircleCI](README.md#Integrate-with-CircleCI)

# API definition

The API consists of these methods:

* GET /api/v1/products
* POST /api/v1/orders
* GET /api/v1/orders/{id}
* GET /health

The endpoints are described below:

### GET /api/v1/products

Returns the list of available products in the shop. Use the id of the products you desire to make an order. The expected result is here:

```javascript
[
    {
        "id": 1,
        "name": "Amazon Kindle 10th gen",
        "price": "90.0",
        "created_at": "2020-02-22T19:51:00.559Z",
        "updated_at": "2020-02-22T19:51:00.559Z"
    },
    {
        "id": 2,
        "name": "Alexa virtual assistant",
        "price":"50.0",
        "created_at": "2020-02-22T19:51:00.841Z",
        "updated_at": "2020-02-22T19:51:00.841Z"
    }
]
```

### POST /api/v1/orders

Makes an order including the selected products with its quantities and other order details. The expected payload is described below:

```javascript
{
    "order": {
        "customer_name": "Sherlock Holmes",
        "phone": "123123123",
        "address": "Baker street, London",
        "payment_method": "MASTER_CARD",
        "card_number": "1234123412341234",
        "items": [
            {"product_id": 1, "quantity": 2}, {"product_id": 2, "quantity": 1}
        ]
    }
}
```

In this call the customer is requesting 2 products of the id 1 (Amazon Kindle 10th gen) and 1 product of the id 2 (Alexa virtual assistant). The fields are explained below:

* **customer_name**: Name of the customer. Mandatory.
* **phone**: Phone number of the customer. Mandatory. Must be a valid number between 9 and 15 digits
* **address**: Customer's address to deliver the products. Mandatory
* **payment_method**: Payment method used during payment. Mandatory. Possible values: VISA, PAYPAL, MASTER_CARD
* **card_number**: Card number used for payment. Mandatory. Must have between 16 and 20 digits
* **items**: List of the desired product ids with its quantities. Mandatory

Provided that the API is deployed in **monkeyshop.davidverduojeda.info**, the curl command to create an order would be this one:

```bash
curl -X POST -v -H "Content-Type: application/json" -d '{"order":{"customer_name":"Sherlock Holmes","phone":"123123123","address":"Baker street, London","payment_method":"MASTER_CARD","card_number":"1234123412341234","items":[{"product_id":1,"quantity":2},{"product_id":2,"quantity":1}]}}' http://monkeyshop.davidverduojeda.info/api/v1/orders
```

The endpoint response will be a **201 Created** status code with a body like this:

```javascript
{
    "id": 1,
    "customer_name":"Sherlock Holmes",
    "phone":"123123123",
    "address":"Baker street, London",
    "total":"230.0",
    "payment_method":"MASTER_CARD",
    "created_at":"2020-04-25T15:31:11.500Z",
    "card_number":"XXXX-XXXX-XXXX-1234",
    "items":[
        {"product_id":1,"quantity":2,"name":"Amazon Kindle 10th gen","price":"90.0","line_price":"180.0"},
        {"product_id":2,"quantity":1,"name":"Alexa virtual assistant","price":"50.0","line_price":"50.0"}
    ]
}
```
Where the order **id** is returned, as well as the total amount of the purchase. This **id** could be used in the GET REST endpoint to get the order content.

If during the call a validation rule is not fulfilled, the endpoint would return a **422 Unprocessable Entity**, displaying the errors in the body. Here it comes a sample invalid response:

```javascript
{"phone":["can't be blank","is too short (minimum is 9 characters)","is invalid"]}
```

### GET /api/v1/orders/{id}

This endpoint displays the details of an Order provided its **id**. A response for the recently created order would be this, with a **200 Ok** status code:

```javascript
{
    "id":1,
    "customer_name":"Sherlock Holmes",
    "phone":"123123123",
    "address":"Baker street, London",
    "total":"230.0",
    "payment_method":"MASTER_CARD",
    "created_at":"2020-04-25T15:31:11.500Z",
    "card_number":"XXXX-XXXX-XXXX-1234",
    "items":[
        {"product_id":1,"quantity":2,"name":"Amazon Kindle 10th gen","price":"90.0","line_price":"180.0"},
        {"product_id":2,"quantity":1,"name":"Alexa virtual assistant","price":"50.0","line_price":"50.0"}
    ]
}
```

### GET /health

This method is used by Kubernetes to determine if the Pod container is running properly. When the application is working correctly, the **200 Ok** status code is followed by this body:

```javascript
{"success":true}
```

# Implementation details

The application consists of a Ruby On Rails backend which implements the API. This is configured to run in Kubernetes and rely on a CloudSQL PostgreSQL database. Ruby on Rails is based on the MVC pattern, which distributes classes in a very maintainable way. The project files are described below:

* **app/**: contains the applitation main classes
- **app/models**: Contains the model classes: **Product**, **Order** and **LineItem**.
- **app/controllers**: Contains the controller classes: **Api::V1::OrdersController**, **Api::V1::ProductsController** and **HealthController**
- **app/views**: Contains the jbuilder view templates for the JSON responses
* **config/**: Contains the Rails configuration files
- **config/database.yml**: Database conf file, ready to get values from environment variables
- **config/routes.rb**: Definition of the valid routes and its mapping to controllers
* **db/**: Contains the database migration scripts, the schema definition and the database population script
* **k8s/**: Contains the Kubernetes configuration files to deploy the application
* **spec/**: RSpec unit and integration tests for the API
* **Gemfile** and **Gemfile.lock**: files to define the Ruby dependencies
* **Dockerfile**: docker file that allows to build the application container
* **docker-compose.yml**: Docker Compose conf file to run the application with a local database
* **docker-compose-cloud-template.yml**: Docker Compose conf template file to run the application with a CloudSQL database
* **.circleci/config.yml**: CircleCI pipeline configuration file

# Running the application locally with docker compose

To run the application locally, you need Docker and Docker compose installed in your system. Then, follow the next steps:

### Build docker container image (Optional, Docker compose will build it if not present)

```bash
docker build -t shop .
```

### Initialize database schema (only the first time)

```bash 
docker-compose run -e RAILS_ENV=production shop rake db:create db:migrate
docker-compose run -e RAILS_ENV=development shop rake db:create db:migrate
docker-compose run -e RAILS_ENV=test shop rake db:create db:migrate
```

### Populate database with products (only the first time)

```bash 
docker-compose run -e RAILS_ENV=production shop rake db:seed
docker-compose run -e RAILS_ENV=development shop rake db:seed
```

### Run the RSpec tests

```bash
docker-compose run -e RAILS_ENV=test shop bundle exec rspec spec
```

you can also run a subset of the tests, like here:

```bash
docker-compose run -e RAILS_ENV=test shop bundle exec rspec spec/models
```

```bash
docker-compose run -e RAILS_ENV=test shop bundle exec rspec spec/controllers
```

```bash
docker-compose run -e RAILS_ENV=test shop bundle exec rspec spec/requests
```

### Run the application

```bash
docker-compose up
```

The application will be available in localhost:3000

# Create a CloudSQL instance and a Service Account

To deploy our application in the cloud, we will use a Kubernetes cluster to run our application container and a managed database. For this example I chose Google Cloud Platform, but other cloud vendor could fit in the solution. 

In this section we are going to create a PostgreSQL CloudSQL instance. Then we will create a Service Account to connect to the database from outside GCP and inside GKE.

### Create a Google Cloud Platform project

The first step is to go to the GCP console and create a new project. Let's say that our project identifier is monkey-test-9876

### Create a CloudSQL instance and a Service Account

We need to go to the CloudSQL section and press in *Create instance* button. Then we choose PostgreSQL and then fill out these fields:

* **Database version**: PostgreSQL 11
* **Region**: europe-west1 (choose a near location) 
* **Zone**: europe-west1-b (choose a near location)
* **Instance ID**: shop-database
* **Password**: enter a suitable password for posgres user and remember it

![picture](readme/create_instance.png)

Once you press OK, the instance will be created. When you press in the overview of the instance, you can see the **Instance connection id**. Write it somewhere, since it will be required to connect.

![picture](readme/cloud_sql_instance.png)

### Create a database in the instance for each environment

Go to databases section and create the following databases:

* shop
* shop_development
* shop_test

![picture](readme/databases.png)

### Create a Service Account to connect to the database

We need to go to **IAM** section and then to **Service Accounts**. We press on *Create Service Account* button.

![picture](readme/create_service_account.png)

We fill out the form and press create. Then we select the **Cloud SQL Editor** role and press continue.

![picture](readme/service_account_role.png)

Finally, we need to create a Key. We press on *Create key* button, choose JSON file and create. A **key JSON file** should be downloaded. Save this file because it's necessary to connect to the database. The service account should be created. We have the JSON key file, the database instance id and the instance password.

### Enable CloudSQL Admin API

You need to go to **APIs & Services** section and enable **Cloud SQL Admin** API. This will enable the CloudSQL Proxy to work properly.

# Running the application locally with a database in CloudSQL

We can test our CloudSQL database from our local machine. To do that, copy this docker compose template to a new file:

```bash
cp docker-compose-cloud-template.yml docker-compose-cloud.yml
```

Then edit the new file and include the next info:

* **[CLOUD_SQL_INSTANCE]**: Replace this with the CloudSQL **instance connection id**
* **[PUT_DB_PASSWORD_FOR_POSTGRES_USER]**: replace this with the PosgreSQL password you entered
* **[PATH_TO_SERVICE_ACCOUNT_CREDENTIALS_JSON_FILE]**: replace this with the path of the Service Account Key json file you downloaded

### Initialize database schema in CloudSQL (only the first time)

```bash 
docker-compose -f docker-compose-cloud.yml run -e RAILS_ENV=production shop rake db:create db:migrate
docker-compose -f docker-compose-cloud.yml run -e RAILS_ENV=development shop rake db:create db:migrate
docker-compose -f docker-compose-cloud.yml run -e RAILS_ENV=test shop rake db:create db:migrate
```

### Populate database with products in CloudSQL (only the first time)

```bash 
docker-compose -f docker-compose-cloud.yml run -e RAILS_ENV=production shop rake db:seed
docker-compose -f docker-compose-cloud.yml run -e RAILS_ENV=development shop rake db:seed
```

### Run the RSpec tests using CloudSQL

```bash
docker-compose -f docker-compose-cloud.yml run -e RAILS_ENV=test shop bundle exec rspec spec
```

### Run the application

```bash
docker-compose -f docker-compose-cloud.yml up
```

# Running the application in Kubernetes

To run the application in Kubernetes, you need the CloudSQL instance configured as it was explained above. Then, you need to
upload the container image to a container repository. In this case we will use Docker Hub (provided that my username is davidverdu):

```bash
docker build -t monkeyshop .
docker tag monkeyshop davidverdu/monkeyshop:1.0
docker push davidverdu/monkeyshop:1.0
```

The image will be available here: docker.io/davidverdu/monkeyshop:1.0
This url must be placed in the k8s/deployment.yml file (more details below)

### Create GKE cluster

Go to **Kubernetes Engine** section in GCP console. Then press on **Create Cluster**. Fill out the form with a cluster name and choose the same 
zone as the CloudSQL instance.

![picture](readme/create_cluster.png)

Then, go to default-pool section and select the number of nodes (3 is recommended but we chose 1 for the demo). Press on create. You should see your cluster created.

![picture](readme/clusters.png)

### Edit config files

Go to k8s/configmap.yml and set the env variable CLOUD_SQL_INSTANCE to the CloudSQL instance id. Then copy the secrets template to a new secrets file:

```bash
cp k8s/secrets-template.yml k8s/secrets.yml
```
Then set the DATABASE_PASS env variable with the PostgreSQL password

### Set the created cluster as the default cluster for kubectl

For this you need the Google Cloud SDK installed.

```bash
gcloud container clusters get-credentials monkeyshop-cluster --zone europe-west1-b 
```

You can check that the cluster is correctly set with this commnad:

```bash
kubectl get nodes
```

### Deploy in Kubernetes

First, deploy the config map:

```bash
kubectl apply -f k8s/configmap.yml
```

And create the secrets. For the service account token you need to provide the JSON key file path.

```bash
kubectl apply -f k8s/secrets.yml
kubectl create secret generic service-account-token --from-file=credentials.json=$HOME/gcloud_credentials/monkey-test-6789-1b1c96278c8a.json
```

Create the Kubernetes Deployment 

```bash
kubectl apply -f k8s/deployment.yml
```

Create the Service pointing to our deployment

```bash
kubectl apply -f k8s/service-load-balancer.yml
```

If you want to make tests on a MiniKube cluster, you can use k8s/service.yml instead

### Check deployment

You can check the deployment with these commands:

```bash
kubectl get pods
kubectl get deployments
kubectl get services
```

The last command provides the External IP of the Load Balancer. It can be used to connect to the deployed application.

# Integrate with CircleCI

The first thing you need is to create a Service Account with the **Kubernetes Engine Developer** role. Create a key in JSON format and download it in a safe place.

Then, you will need to set the following environment variables in CircleCI

![picture](readme/circleci_env.png)

You will need to set the credentials of Docker Hub and put the service account key file content into **GCLOUD_SERVICE_KEY** env variable. The pipeline will be launched when you push in the 'release' branch.