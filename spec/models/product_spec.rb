require 'rails_helper'

RSpec.describe Product, type: :model do
  before do
    @product = Product.new name: "New Product", price: 10.0
  end

  subject { @product } 
  it { should respond_to(:name)}
  it { should respond_to(:price)}
  it { should respond_to(:line_items)}
  it { should be_valid}

  describe "when name is not present" do
    before { @product.name = " " }
    it { should_not be_valid }
  end

  describe "when price" do
    describe "is not present" do
      before { @product.price = nil }
      it { should_not be_valid }
    end
    describe "is 0" do
      before { @product.price = 0 }
      it { should_not be_valid }
    end
    describe "is negative" do
      before { @product.price = -1 }
      it { should_not be_valid }
    end
  end

  describe "when name is not unique" do
    before { Product.create name: "New Product", price: 10.0 }
    it { should_not be_valid }
  end
end
