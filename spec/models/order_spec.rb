require 'rails_helper'

RSpec.describe Order, type: :model do
  before do
    @product  = Product.create name: "Kindle", price: 99.0
    @product1 = Product.create name: "Kindle Paperwhite", price: 200.0
    @product2 = Product.create name: "Alexa AI", price: 120.0
    @order = Order.new customer_name: "John Doe", phone: "555555555", address: "Peter street", payment_method: "VISA",
                       card_number: "1234123412341234",
                       items: [{product_id: @product1.id, quantity: 1}]
    
  end

  subject { @order } 
  it { should respond_to(:total)}
  it { should respond_to(:customer_name)}
  it { should respond_to(:phone)}
  it { should respond_to(:address)}
  it { should respond_to(:payment_method)}
  it { should respond_to(:card_number)}
  it { should respond_to(:line_items)}
  it { should respond_to(:items)}
  it { should respond_to(:products)}
  it { should respond_to(:masked_card_number)}

  it { should be_valid }

  describe "when items are not present" do
    before { @order.items = nil }
    it { should_not be_valid }
  end

  describe "when items are empty" do
    before { @order.items = [] }
    it { should_not be_valid }
  end

  describe "when customer_name is not present" do
    before { @order.customer_name = " " }
    it { should_not be_valid }
  end

  describe "when phone" do
    describe "is not present" do
      before { @order.phone = " " }
      it { should_not be_valid }
    end
    describe "is too short" do
      before { @order.phone = "123" }
      it { should_not be_valid }
    end
    describe "is too long" do
      before { @order.phone = "123456789111123123" }
      it { should_not be_valid }
    end
    describe "contains bad characters" do
      before { @order.phone = "123456ABC" }
      it { should_not be_valid }
    end
    describe "contains + char" do
      before { @order.phone = "+34 123123123" }
      it { should be_valid }
    end
    describe "contains spaces" do
      before { @order.phone = "123 123 123" }
      it { should be_valid }
    end
  end

  describe "when payment_method" do
    describe "is not present" do
      before { @order.payment_method = " " }
      it { should_not be_valid }
    end
    describe "is invalid" do
      before { @order.payment_method = "INVALID" }
      it { should_not be_valid }
    end
    describe "is VISA" do
      before { @order.payment_method = "VISA" }
      it { should be_valid }
    end
    describe "is PAYPAL" do
      before { @order.payment_method = "PAYPAL" }
      it { should be_valid }
    end
    describe "is MASTER_CARD" do
      before { @order.payment_method = "MASTER_CARD" }
      it { should be_valid }
    end
  end

  describe "when card number" do
    describe "is not present" do
      before { @order.card_number = " " }
      it { should_not be_valid }
    end
    describe "is too short" do
      before { @order.card_number = "123123123123" }
      it { should_not be_valid }
    end
    describe "is too long" do
      before { @order.card_number = "123412341234123412121212" }
      it { should_not be_valid }
    end
    describe "contains bad characters" do
      before { @order.card_number = "1234123412ABCDEFG" }
      it { should_not be_valid }
    end
  end

  describe "when save order" do
    before { @order.save }
    it "displays correct total" do
      expect(@order.line_items.count).to eq(1)
      expect(@order.total).to eq(200.0)
      expect(@order.products).to include(@product1)
    end
    it "displays correct masked card number" do
      expect(@order.masked_card_number).to eq("XXXX-XXXX-XXXX-1234")
    end
  end

  describe "when order contains many products" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: 1}] }
    it { should be_valid }
    describe "saved line items" do
      before { @order.save }
      it "displays correct total" do
        expect(@order.total).to eq(520.0)
        expect(@order.line_items.count).to eq(2)
        expect(@order.products).to include(@product1)
        expect(@order.products).to include(@product2)
      end
    end
  end

  describe "when order contains invalid products" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id+1000, quantity: 1}] }
    it { should_not be_valid }
  end

  describe "when order contains repeated products" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: 1}, 
                             {product_id: @product2.id, quantity: 3}] }
    it { should_not be_valid }
  end

  describe "when order contains products without quantity" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id}] }
    it { should_not be_valid }
  end

  describe "when order contains products bad quantity" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: "bad"}] }
    it { should_not be_valid }
  end

  describe "when order contains products zero quantity" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: 0}] }
    it { should_not be_valid }
  end

  describe "when order contains products negative quantity" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: -1}] }
    it { should_not be_valid }
  end

  describe "when order contains products float quantity" do
    before { @order.items = [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: 1.5}] }
    it { should_not be_valid }
  end
end
