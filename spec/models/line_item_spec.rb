require 'rails_helper'

RSpec.describe LineItem, type: :model do
  before do
    @product1 = Product.create name: "Kindle", price: 99.0
    @product2 = Product.create name: "Alexa", price: 200.0
    @order = Order.create customer_name: "John Doe", phone: "555555555", address: "Peter street", payment_method: "VISA", total: 99.0,
                          card_number: "1234123412341234",
                          items: [{product_id: @product1.id, quantity: 1}]
    @line_item = LineItem.new product_id: @product2.id, quantity: 1, order_id: @order.id
  end

  subject { @line_item } 
  it { should respond_to(:order)}
  it { should respond_to(:product)}
  it { should respond_to(:quantity)}

  it { should be_valid }

  describe "when order is not present" do
    before { @line_item.order = nil }
    it { should_not be_valid }
  end

  describe "when product is not present" do
    before { @line_item.product = nil }
    it { should_not be_valid }
  end

  describe "when line item is not unique" do
    before { @line_item2 = LineItem.create product_id: @product2.id, quantity: 1, order_id: @order.id }
    it { should_not be_valid }
  end

  describe "when quantity" do
    describe "is not present" do
      before { @line_item.quantity = nil }
      it { should_not be_valid }
    end

    describe "is 0" do
      before { @line_item.quantity = 0 }
      it { should_not be_valid }
    end

    describe "is -1" do
      before { @line_item.quantity = -1 }
      it { should_not be_valid }
    end

    describe "is >= 50" do
      before { @line_item.quantity = 50 }
      it { should_not be_valid }
    end

    describe "is 1.5" do
      before { @line_item.quantity = 1.5 }
      it { should_not be_valid }
    end

    describe "is a string" do
      before { @line_item.quantity = "one" }
      it { should_not be_valid }
    end
  end
  
end
