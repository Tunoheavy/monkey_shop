require 'rails_helper'

RSpec.describe "Products", type: :request do
  before do
    @p1 = Product.create name: "product1", price: 20.0
    @p2 = Product.create name: "product2", price: 25.0
  end
  
  
  
  describe "GET /products" do
    it "displays available products" do
      get api_v1_products_path
      expect(response).to have_http_status(200)
      expect(response.content_type).to eq("application/json; charset=utf-8")
      parsed_body = JSON.parse(response.body)
      expect(parsed_body.length).to eq(2)
    end
  end
end
