require 'rails_helper'

RSpec.describe "Orders", type: :request do

  describe "GET /orders/1" do
    describe "of a simple order" do
      before do
        @product = Product.create name: "Kindle", price: 99.0
        @order = Order.create customer_name: "John Doe", phone: "555555555", address: "Peter street", payment_method: "VISA",
                              card_number: "1234123412341234",
                              items: [{product_id: @product.id, quantity: 1}]
      end
      it "works! (now write some real specs)" do
        get api_v1_order_path(@order)
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq("application/json; charset=utf-8")
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['id']).to eq(@order.id)
        expect(parsed_body['customer_name']).to eq("John Doe")
        expect(parsed_body['phone']).to eq("555555555")
        expect(parsed_body['address']).to eq("Peter street")
        expect(parsed_body['payment_method']).to eq("VISA")
        expect(parsed_body['card_number']).to eq("XXXX-XXXX-XXXX-1234")
        expect(parsed_body['total']).to eq("99.0")
        expect(parsed_body['items'].length).to eq(1)
        expect(parsed_body['items'].first['product_id']).to eq(@product.id)
        expect(parsed_body['items'].first['quantity']).to eq(1)
        expect(parsed_body['items'].first['name']).to eq("Kindle")
        expect(parsed_body['items'].first['price']).to eq("99.0")
        expect(parsed_body['items'].first['line_price']).to eq("99.0")
      end
    end
  
    describe "of a complex order" do
      before do
        @product1 = Product.create name: "Kindle Paperwhite", price: 100.0
        @product2 = Product.create name: "Alexa bot", price: 200.0
        @order = Order.create customer_name: "John Doe", phone: "555555555", address: "Peter street", payment_method: "VISA",
                              card_number: "1234123412341234",
                              items: [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: 1}]
      end
      it "works! (now write some real specs)" do
        get api_v1_order_path(@order)
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq("application/json; charset=utf-8")
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['id']).to eq(@order.id)
        expect(parsed_body['customer_name']).to eq("John Doe")
        expect(parsed_body['phone']).to eq("555555555")
        expect(parsed_body['address']).to eq("Peter street")
        expect(parsed_body['payment_method']).to eq("VISA")
        expect(parsed_body['card_number']).to eq("XXXX-XXXX-XXXX-1234")
        expect(parsed_body['total']).to eq("400.0")
        expect(parsed_body['items'].length).to eq(2)
        expect(parsed_body['items'].first['product_id']).to eq(@product1.id)
        expect(parsed_body['items'].first['quantity']).to eq(2)
        expect(parsed_body['items'].first['price']).to eq("100.0")
        expect(parsed_body['items'].first['name']).to eq("Kindle Paperwhite")
        expect(parsed_body['items'].first['line_price']).to eq("200.0")
        expect(parsed_body['items'].last['product_id']).to eq(@product2.id)
        expect(parsed_body['items'].last['quantity']).to eq(1)
        expect(parsed_body['items'].last['price']).to eq("200.0")
        expect(parsed_body['items'].last['name']).to eq("Alexa bot")
        expect(parsed_body['items'].last['line_price']).to eq("200.0")
      end
    end
  end
  
  describe "POST /api/v1/orders" do
    before do
      @product1 = Product.create name: "Kindle Paperwhite", price: 100.0
      @product2 = Product.create name: "Alexa bot", price: 200.0
    end
    describe "of a simple order" do
      let(:order_ok) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "MASTER_CARD",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id, quantity: 2}]
        }
      }
      it "creates the order" do
        post api_v1_orders_path, params: {order: order_ok}
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq("application/json; charset=utf-8")
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['customer_name']).to eq("Sherlock Holmes")
        expect(parsed_body['phone']).to eq("123123123")
        expect(parsed_body['address']).to eq("Baker street, London")
        expect(parsed_body['payment_method']).to eq("MASTER_CARD")
        expect(parsed_body['card_number']).to eq("XXXX-XXXX-XXXX-1234")
        expect(parsed_body['total']).to eq("200.0")
        expect(parsed_body['items'].length).to eq(1)
        expect(parsed_body['items'].first['product_id']).to eq(@product1.id)
        expect(parsed_body['items'].first['quantity']).to eq(2)
        expect(parsed_body['items'].first['price']).to eq("100.0")
        expect(parsed_body['items'].first['name']).to eq("Kindle Paperwhite")
        expect(parsed_body['items'].first['line_price']).to eq("200.0")
      end
      it "does create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_ok}
        }.to change(Order, :count).by(1)
      end
    end
    describe "of a complex order" do
      let(:order_complex) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "MASTER_CARD",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id, quantity: 2}, {product_id: @product2.id, quantity: 1}]
        }
      }
      it "creates the order" do
        post api_v1_orders_path, params: {order: order_complex}
        expect(response).to have_http_status(201)
        expect(response.content_type).to eq("application/json; charset=utf-8")
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['customer_name']).to eq("Sherlock Holmes")
        expect(parsed_body['phone']).to eq("123123123")
        expect(parsed_body['address']).to eq("Baker street, London")
        expect(parsed_body['payment_method']).to eq("MASTER_CARD")
        expect(parsed_body['card_number']).to eq("XXXX-XXXX-XXXX-1234")
        expect(parsed_body['total']).to eq("400.0")
        expect(parsed_body['items'].length).to eq(2)
        expect(parsed_body['items'].first['product_id']).to eq(@product1.id)
        expect(parsed_body['items'].first['quantity']).to eq(2)
        expect(parsed_body['items'].first['price']).to eq("100.0")
        expect(parsed_body['items'].first['name']).to eq("Kindle Paperwhite")
        expect(parsed_body['items'].first['line_price']).to eq("200.0")
        expect(parsed_body['items'].last['product_id']).to eq(@product2.id)
        expect(parsed_body['items'].last['quantity']).to eq(1)
        expect(parsed_body['items'].last['price']).to eq("200.0")
        expect(parsed_body['items'].last['name']).to eq("Alexa bot")
        expect(parsed_body['items'].last['line_price']).to eq("200.0")
      end
      it "does create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_complex}
        }.to change(Order, :count).by(1)
      end
    end

    describe "invalid order payload" do

      describe "of an order with invalid payment method" do
        let(:order_invalid_payment) {
          {
            customer_name: "Sherlock Holmes",
            phone: "123123123",
            address: "Baker street, London",
            payment_method: "INVALID",
            card_number: "1234123412341234",
            items: [{product_id: @product1.id, quantity: 2}]
          }
        }
        it "cancel order creation" do
          post api_v1_orders_path, params: {order: order_invalid_payment}
          expect(response).to have_http_status(422)
          expect(response.content_type).to eq("application/json; charset=utf-8")
        end
        it "does not create a new Order" do
          expect {
            post api_v1_orders_path, params: {order: order_invalid_payment}
          }.to change(Order, :count).by(0)
        end
      end

      describe "of an order with invalid card number too short" do
        let(:order_invalid_card_too_short) {
          {
            customer_name: "Sherlock Holmes",
            phone: "123123123",
            address: "Baker street, London",
            payment_method: "VISA",
            card_number: "123",
            items: [{product_id: @product1.id, quantity: 2}]
          }
        }
        it "cancel order creation" do
          post api_v1_orders_path, params: {order: order_invalid_card_too_short}
          expect(response).to have_http_status(422)
          expect(response.content_type).to eq("application/json; charset=utf-8")
        end
        it "does not create a new Order" do
          expect {
            post api_v1_orders_path, params: {order: order_invalid_card_too_short}
          }.to change(Order, :count).by(0)
        end
      end

      describe "of an order with invalid card number ivalid" do
        let(:order_invalid_card_invalid) {
          {
            customer_name: "Sherlock Holmes",
            phone: "123123123",
            address: "Baker street, London",
            payment_method: "VISA",
            card_number: "1234123412341234ABC",
            items: [{product_id: @product1.id, quantity: 2}]
          }
        }
        it "cancel order creation" do
          post api_v1_orders_path, params: {order: order_invalid_card_invalid}
          expect(response).to have_http_status(422)
          expect(response.content_type).to eq("application/json; charset=utf-8")
        end
        it "does not create a new Order" do
          expect {
            post api_v1_orders_path, params: {order: order_invalid_card_invalid}
          }.to change(Order, :count).by(0)
        end
      end

      describe "of an order with invalid card number too long" do
        let(:order_invalid_card_too_long) {
          {
            customer_name: "Sherlock Holmes",
            phone: "123123123",
            address: "Baker street, London",
            payment_method: "VISA",
            card_number: "123412341234123412341234123412341234",
            items: [{product_id: @product1.id, quantity: 2}]
          }
        }
        it "cancel order creation" do
          post api_v1_orders_path, params: {order: order_invalid_card_too_long}
          expect(response).to have_http_status(422)
          expect(response.content_type).to eq("application/json; charset=utf-8")
        end
        it "does not create a new Order" do
          expect {
            post api_v1_orders_path, params: {order: order_invalid_card_too_long}
          }.to change(Order, :count).by(0)
        end
      end

      describe "of an order with empty customer name" do
        let(:order_invalid_empty_customer_name) {
          {
            customer_name: " ",
            phone: "123123123",
            address: "Baker street, London",
            payment_method: "VISA",
            card_number: "1234123412341234",
            items: [{product_id: @product1.id, quantity: 2}]
          }
        }
        it "cancel order creation" do
          post api_v1_orders_path, params: {order: order_invalid_empty_customer_name}
          expect(response).to have_http_status(422)
          expect(response.content_type).to eq("application/json; charset=utf-8")
        end
        it "does not create a new Order" do
          expect {
            post api_v1_orders_path, params: {order: order_invalid_empty_customer_name}
          }.to change(Order, :count).by(0)
        end
      end
    end

    describe "of an order with empty address" do
      let(:order_invalid_empty_address) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: " ",
          payment_method: "VISA",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id, quantity: 2}]
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_empty_address}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_empty_address}
        }.to change(Order, :count).by(0)
      end
    end

    describe "of an order without items" do
      let(:order_invalid_phone) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "VISA",
          card_number: "1234123412341234"
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_phone}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_phone}
        }.to change(Order, :count).by(0)
      end
    end

    describe "of an order with empty items" do
      let(:order_invalid_empty_items) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "VISA",
          card_number: "1234123412341234",
          items: []
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_empty_items}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_empty_items}
        }.to change(Order, :count).by(0)
      end
    end

    describe "of an order with invalid product id" do
      let(:order_invalid_product_id) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "VISA",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id+1000, quantity: 2}]
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_product_id}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_product_id}
        }.to change(Order, :count).by(0)
      end
    end

    describe "of an order with invalid zero quantity" do
      let(:order_invalid_zero_quantity) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "VISA",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id, quantity: 0}]
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_zero_quantity}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_zero_quantity}
        }.to change(Order, :count).by(0)
      end
    end

    describe "of an order with invalid negative quantity" do
      let(:order_invalid_negative_quantity) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "VISA",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id, quantity: -1}]
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_negative_quantity}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_negative_quantity}
        }.to change(Order, :count).by(0)
      end
    end

    describe "of an order with invalid float quantity" do
      let(:order_invalid_float_quantity) {
        {
          customer_name: "Sherlock Holmes",
          phone: "123123123",
          address: "Baker street, London",
          payment_method: "VISA",
          card_number: "1234123412341234",
          items: [{product_id: @product1.id, quantity: 1.5}]
        }
      }
      it "cancel order creation" do
        post api_v1_orders_path, params: {order: order_invalid_float_quantity}
        expect(response).to have_http_status(422)
        expect(response.content_type).to eq("application/json; charset=utf-8")
      end
      it "does not create a new Order" do
        expect {
          post api_v1_orders_path, params: {order: order_invalid_float_quantity}
        }.to change(Order, :count).by(0)
      end
    end
  end
end
