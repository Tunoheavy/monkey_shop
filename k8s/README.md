Put in configmap.yml the CloudSQL instance name in the env variable

Run: kubectl apply -f configmap.yml

Ensure that secrets.yml contains the correct DB password for user posgres

Run: kubectl apply -f secrets.yml

Create the secret with the Service Account credentials. The Service Account must have permissions to access Cloud SQL instances.

RUN: kubectl create secret generic service-account-token --from-file=credentials.json=$HOME/gcloud_credentials/monkey-test-1234-1b1c96278c8a.json

Create the Kubernetes Deployment 

RUN: kubectl apply -f deployment.yml

Create the Service pointing to our deployment

RUN: kubectl apply -f service-load-balancer.yml