Rails.application.routes.draw do
  get 'health', controller: 'health'

  namespace :api do
    namespace :v1 do
      resources :orders, only: [:create, :show]
      resources :products, only: :index
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
