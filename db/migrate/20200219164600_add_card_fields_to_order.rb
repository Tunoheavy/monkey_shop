class AddCardFieldsToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :card_number, :string
  end
end
