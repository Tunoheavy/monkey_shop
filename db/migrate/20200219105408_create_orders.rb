class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.decimal :total
      t.text :address
      t.string :payment_method

      t.timestamps
    end
  end
end
