json.(order, :id, :customer_name, :phone, :address, :total, :payment_method, :created_at)
json.card_number order.masked_card_number

json.items order.line_items do |line_item|
  json.(line_item, :product_id, :quantity)
  json.name line_item.product.name
  json.price line_item.product.price
  json.line_price (line_item.product.price * line_item.quantity)
end