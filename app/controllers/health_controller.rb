class HealthController < ApplicationController
  #GET /health
  def health
    @count = Order.count
    render status: :ok, json: {success: true}
  end
end
