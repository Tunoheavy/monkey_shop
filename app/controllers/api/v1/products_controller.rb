class Api::V1::ProductsController < ApplicationController

  # GET /products
  def index
    @products = Product.all

    render json: @products
  end

end
