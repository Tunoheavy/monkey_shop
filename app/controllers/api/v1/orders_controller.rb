class Api::V1::OrdersController < ApplicationController
  before_action :set_order, only: [:show]

  # GET /api/v1/orders/1
  def show
    render status: :ok, content_type: "application/json"
  end

  # POST /api/v1/orders
  def create
    @order = Order.new(order_params)

    if @order.save
      @order.reload
      render partial: "order", locals: {order: @order}, status: :created, 
             location: api_v1_order_url(@order), content_type: "application/json"
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:address, :payment_method, :customer_name, :phone, :card_number,
                                    {items: [[:product_id, :quantity]]})
    end
end
