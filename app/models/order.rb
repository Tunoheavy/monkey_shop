class Order < ApplicationRecord
  has_many :line_items
  has_many :products, through: :line_items
  validates :customer_name, presence: true
  validates :card_number, presence: true, length: { in: 16..20 }, format: { with: /\A[\d]+\z/ }
  validates :phone, presence: true, length: { in: 9..15 }, format: { with: /\A\+?\d[\d\s]+\z/ }
  validates :address, presence: true

  PAYMENT_METHODS = %w(VISA PAYPAL MASTER_CARD)
  validates :payment_method, presence: true, inclusion: { in: PAYMENT_METHODS }

  attr_writer :items
  def items
    return @items if @items
    line_items.ids
  end
  validates :items, presence: true, on: :create
  validate :items_validation
  after_save :assign_items
  
  def masked_card_number
    "XXXX-XXXX-XXXX-#{card_number.slice(-4..-1)}"  
  end
  
  private

  def items_validation
    return true unless @items
    unless @items.kind_of?(Array)
      errors.add(:items, :array_expected)
      return false
    end

    products_to_assign = Product.where(id: @items.map {|p| p[:product_id] })
    errors.add(:items, :at_least_one_item_expected) if products_to_assign.empty?
    errors.add(:items, :existing_product_correspondence_expected) if products_to_assign.length != @items.length
    quantities_to_assign = @items.map {|p| p[:quantity] }
    quantities_to_assign.each do |qty|
      errors.add(:items, :bad_quantity) if qty.to_i < 1 or qty.to_i > 50 or qty.to_f > qty.to_i
    end
    errors.add(:items, :bad_quantity) if quantities_to_assign.length != @items.length
  end

  def assign_items
    total_sum = 0.0
    @items.each do |item|
      product = Product.find item[:product_id]
      line_items.create product_id: product.id, quantity: item[:quantity].to_i
      total_sum += Integer(item[:quantity]) * product.price
    end
    update_attribute(:total, total_sum)
    true
  end

end
