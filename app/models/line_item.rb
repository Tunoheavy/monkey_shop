class LineItem < ApplicationRecord
  belongs_to :order
  belongs_to :product
  validates :order_id, presence: true, uniqueness: { scope: :product_id }
  validates :product_id, presence: true
  validates :quantity, presence: true, numericality: { greater_than: 0, less_than: 50, only_integer: true }
end
